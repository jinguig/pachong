

import java.sql.*;

public class JinSeDao {
    private Connection conn = null;
    private Statement stmt = null;

    public JinSeDao() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/jinsecaijing?user=root&password=root";
            conn = DriverManager.getConnection(url);
            stmt = conn.createStatement();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public int add(JinSeBean jinSeBean) {
        try {
            String sql = "INSERT INTO `jinsecaijing`.`jinsecaijing` (`id`, `title`, `date`, `tags`, `author`,`content`) VALUES (?, ?, ?, ?, ?,?);";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, jinSeBean.getId());
            ps.setString(2, jinSeBean.getTitle());
            ps.setString(3, jinSeBean.getDate());
            ps.setString(4, jinSeBean.getTags());
            ps.setString(5, jinSeBean.getAuthor());
            ps.setString(6, jinSeBean.getContent());
            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int addPicture(PictureBean pictureBean){
        try {
            String sql = "INSERT INTO `jinsecaijing`.`picturestring` (`id`, `imgString`) VALUES (?, ?);";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1,pictureBean.getId());
            ps.setString(2,pictureBean.getImgString());
            return ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return -1;
    }
}

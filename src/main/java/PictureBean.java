public class PictureBean {

    private int id;
    private String imgString;       //图片

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgString() {
        return imgString;
    }

    public void setImgString(String imgString) {
        this.imgString = imgString;
    }

    @Override
    public String toString() {
        return "PictureBean{" +
                "id=" + id +
                ", imgString='" + imgString + '\'' +
                '}';
    }
}
